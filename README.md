# Building Visualization App

Quellcode für meine Projektarbeit *Entwicklung einer Webanwendung zur Visualisierung eines Gebäudeaufbaus mit Three.js*.

Die gebaute Webseite wird [hier](https://waldemarlehner-hrw.gitlab.io/ss-2022/projektarbeit/building-visualization-app) gehostet.

Die schriftliche Ausarbeitung wird [hier](https://waldemarlehner-hrw.gitlab.io/ss-2022/projektarbeit/projektarbeit-tex) gehostet.
