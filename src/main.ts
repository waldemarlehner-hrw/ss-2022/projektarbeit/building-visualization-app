import { createApp } from "vue";
import App from "./App.vue";
import { Quasar } from "quasar";
import router from "./pages/routes";

// Import icon libraries
import "@quasar/extras/material-icons/material-icons.css";
import "@quasar/extras/fontawesome-v5/fontawesome-v5.css";

// Import Quasar css
import "quasar/src/css/index.sass";

createApp(App)
	.use(router)
	.use(Quasar, {
		plugins: {}
	})
	.mount("#app");