import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router";

const routes : RouteRecordRaw[] = [
	{
		path: "/about",
		name: "About",
		component: () => import("./AboutPage.vue")
	},
	{
		path: "/",
		redirect: {name: "App"}
	},
	{
		path: "/app/:path?",
		name: "App",
		component: () => import("./MainPage.vue")
	},
	{
		path: "/:catchAll(.*)",
		redirect: {name: "App"}
	}
];

export default createRouter({
	history: createWebHashHistory(),
	routes
});
