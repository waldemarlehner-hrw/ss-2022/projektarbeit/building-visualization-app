import { Topic } from "../Topic";

export default class DoubleClickHandler {
	public readonly onDoubleClickEvent = new Topic<void>();

	private lastClickTime = 0;

	constructor(private readonly timeOutMillis: number, private readonly doubleClickCooldownMillis: number = 0) {}

	public notifyAboutClick() {

		const now = Date.now();
		const delta = now - this.lastClickTime;
		if (delta < 0) {
			// During cooldown. ignore
			return;
		}

		const emitEvent = delta < this.timeOutMillis;
		this.lastClickTime = now;
		if(emitEvent) {
			this.lastClickTime += this.doubleClickCooldownMillis;
		}

		if (emitEvent) {
			this.onDoubleClickEvent.invoke();
		}
	}


}
