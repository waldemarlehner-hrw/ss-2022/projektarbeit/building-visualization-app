export class Topic<T> {
	clear() {
		this.listeners = [];
	}
	private listeners: ((event: T) => void)[] = [];

	public addListener(listener: (event: T) => void) {
		this.listeners.push(listener);
	}

	public removeListener(listener: (event: T) => void) : boolean {
		const index = this.listeners.indexOf(listener);
		if(index > -1) {
			this.listeners.splice(index, 1);
			return true;
		}
		return false;
	}

	public invoke(event: T) {
		for(const listener of this.listeners) {
			listener(event);
		}
	}
}