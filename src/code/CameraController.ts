import { Camera, Event, Object3D, Raycaster, Vector2, Mesh, Vector3 } from "three";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import { IUpdateable } from "./interfaces/IUpdateable";
import { SceneNode } from "./sceneschema/SceneNode";
import { Topic } from "./Topic";
import { MeshBVH, acceleratedRaycast } from "three-mesh-bvh";
/// Override THREE.js Intersector
Mesh.prototype.raycast = acceleratedRaycast;

///


export interface ICameraIntersector {
	setNewIntersectables(data: Object3D[]) : void;
}

export class CameraController implements IUpdateable, ICameraIntersector {


	private _controls : OrbitControls;

	public readonly currentlyHoveringSceneNodeTopic: Topic<SceneNode|undefined> = new Topic();
	public readonly voidClickedEvent: Topic<void> = new Topic();
	private rayCaster: Raycaster;

	public elementClickedEvent = new Topic<SceneNode>();

	private elementsToIntersect: Object3D[] = [];

	// Mouse Handling
	private wasClicked = false;

	// This tells if the Mouse Cursor is being displayed instead of the regular mouse cursor.
	private isPointerShown = false;


	private mouseDownTime = 0;
	private mouseDownCursorPos = {x: 0, y: 0};


	constructor(private _camera : Camera, private _rendererDom: HTMLElement) {
		this._controls = new OrbitControls(_camera, _rendererDom);
		this._controls.maxPolarAngle = (Math.PI / 2) - .05;
		this._controls.minPolarAngle = .1;
		this.rayCaster = new Raycaster();
		this.rayCaster.firstHitOnly = true;

		this._controls.enableDamping = true;
		this._controls.dampingFactor = .3;
		// Camera will not go more than 100 units away from Orbiting Origin
		this._controls.maxDistance = 100;
		// Panning will move on XZ-Plane instead of relative to Camera
		this._controls.screenSpacePanning = false;

		this._rendererDom.onpointermove = this.__onPointerMoveEvent;
		this._rendererDom.onpointerdown = this.__onPointerDownEvent;
		this._rendererDom.onpointerup = this.__onPointerUpEvent;
	}

	private buildBvhTree(entry : Object3D) {

		if (entry instanceof Mesh) {
			if(import.meta.env.DEV) {

				console.log("Build BVH for ",entry);
			}
			entry.geometry.boundsTree = new MeshBVH(entry.geometry);
		}

		for( const child of entry.children) {
			this.buildBvhTree(child);
		}

	}

	public setNewIntersectables(data: Object3D<Event>[]): void {
		if(import.meta.env.DEV) {
			console.groupCollapsed("BVH Tree for new Intersectables");
			console.log([...data]);
		}
		data.map( e => this.buildBvhTree(e));
		if(import.meta.env.DEV) {
			console.groupEnd();
		}
		this.elementsToIntersect = data;
	}


	private onPointerUp(event: PointerEvent): any {
		if(event.button !== 0) { // Left Button
			return;
		}
		const deltaTime = event.timeStamp - this.mouseDownTime;
		if (deltaTime > 200) {
			// Not a mouse click. Ignore
			return;
		}
		const mouseDownAsVector = new Vector2(this.mouseDownCursorPos.x, this.mouseDownCursorPos.y);
		const dist = mouseDownAsVector.distanceTo(this.pointerNdc);
		if(dist > .2) {
			return; // Quick drag, not a click
		}

		this.wasClicked = true;
	}

	private onPointerDown(event: PointerEvent): any {
		if(event.button !== 0) { // Left Button
			return;
		}
		this.mouseDownTime = event.timeStamp;
		this.mouseDownCursorPos = {
			x: this.pointerNdc.x,
			y: this.pointerNdc.y
		};
	}



	private handleRayCasts() {
		this.rayCaster.setFromCamera(this.pointerNdc, this._camera);

		// Result contains the Object3D, but we need the SceneNode (or undefined if nothing was clicked)
		const result = this.intersectElements(this.elementsToIntersect);

		// this contains the Scene Node
		const sceneNodeResult = this.findSceneNode(result);

		this.currentlyHoveringSceneNodeTopic.invoke(sceneNodeResult);

		if(this.isPointerShown !== !!sceneNodeResult) {
			this.isPointerShown = !!sceneNodeResult;
			this.updateCursorClassesOnHtmlElement(!!sceneNodeResult);
		}

		// The most specific variant has been found.
		if (this.wasClicked) {
			this.wasClicked = false;
			if(!!sceneNodeResult) {
				this.elementClickedEvent.invoke(sceneNodeResult);
			} else {
				// No target. Clicking into the void
				this.voidClickedEvent.invoke();
			}
		}

	}

	private updateCursorClassesOnHtmlElement(show: boolean) {
		const domElement = this._rendererDom;
		if(show) {
			domElement.classList.add("show-pointer");
		} else {
			domElement.classList.remove("show-pointer");
		}

	}


	private findSceneNode(result: Object3D | undefined) : SceneNode | undefined {
		if(!result) {
			return undefined;
		}
		if(!!result.userData["node"]) {
			return result.userData["node"];
		}
		const parent = result.parent;
		if(!parent) {
			return undefined;
		}
		return this.findSceneNode(parent);
	}

	private intersectElements(elements: Object3D[]) : Object3D | undefined {
		const intersects = this.rayCaster.intersectObjects(elements);
		if(intersects.length === 0) {
			return undefined;
		}
		return (intersects[0].object);
	}

	private pointerNdc = new Vector2();

	private onPointerMove(event: PointerEvent) {
		// Convert to NDC
		this.pointerNdc.x = (event.clientX / this._rendererDom.offsetWidth) * 2 - 1;
		this.pointerNdc.y = -(event.clientY / this._rendererDom.offsetHeight) * 2 + 1;
	}

	public update(_: number): void {
		this._controls.update();
		this.handleRayCasts();
	}

	private __onPointerMoveEvent = ((event : PointerEvent) => this.onPointerMove(event));
	private __onPointerDownEvent = ((event : PointerEvent) => this.onPointerDown(event));
	private __onPointerUpEvent = ((event : PointerEvent) => this.onPointerUp(event));


	public dispose() {
		this._rendererDom.onpointermove = null;
		this._rendererDom.onpointerdown = null;
		this._rendererDom.onpointerup = null;
		this.elementClickedEvent.clear();
		this.voidClickedEvent.clear();
		this._controls.dispose();
	}



}
