import { Color, DirectionalLight, Fog, HemisphereLight, Mesh, MeshLambertMaterial, PlaneGeometry, Scene, } from "three";
import { IUpdateable } from "./interfaces/IUpdateable";
import { SceneDefinition } from "./sceneschema/SceneDefinition";
import { SceneNode } from "./sceneschema/SceneNode";
import { SkyboxHelper } from "./sceneschema/SkyboxHelper";
import { Topic } from "./Topic";

/**
 * Abstracts away Three's Scene. Any Scene manipulations should happen in here
 */
export class SceneWrapper implements IUpdateable {

	public validatePath(path: string[]) : [string[], boolean] {
		return this.sceneDefinition.validatePath(path);
	}
	public setNewSelectedElement(path: string[]) {
		const node = this.sceneDefinition.findNode(path);
		if(!!node) {
			console.info("Selected Node "+node.id);
			node.setActive();
		} else {
			console.error("Failed to find Node by path", path);
		}
	}

	public newSelectableChildrenEvent = new Topic<string[]>();

	constructor(public readonly scene: Scene, private sceneDefinition: SceneDefinition) {
		if(!sceneDefinition.cameraController) {
			throw new Error("Scene Definition has yet to be initialized...");
		}
		// Subscribe to new children selected event
		sceneDefinition.newSelectableChildrenEvent.addListener(e => this.newSelectableChildrenEvent.invoke(e));

		// Add ground
		const groundGeo = new PlaneGeometry(1000, 1000);
		const groundMat = new MeshLambertMaterial({color: 0xAAAAAA});

		const ground = new Mesh(groundGeo, groundMat);
		ground.rotation.x = -Math.PI / 2;
		ground.receiveShadow = true;
		scene.add(ground);

		// Add lighting
		const hemisphereLight = new HemisphereLight(
			new Color().setHSL(.6, 1, .6),
			new Color().setHSL(.09, 1, .75),
			 .4);

		hemisphereLight.position.set(0, 50, 0);

		const dirLight = new DirectionalLight(0xfcf8ae, .8);
		dirLight.castShadow = true;
		dirLight.shadow.mapSize.set(2048, 2048);
		const shadowEmitterSize = 50;
		dirLight.shadow.camera.left = -shadowEmitterSize;
		dirLight.shadow.camera.right = shadowEmitterSize;
		dirLight.shadow.camera.top = shadowEmitterSize;
		dirLight.shadow.camera.bottom = -shadowEmitterSize;
		dirLight.shadow.camera.far = 3500;
		dirLight.shadow.bias = .0001;
		dirLight.position.set(5,7,6);

		scene.fog = new Fog(0xEAEAEA, 10, 100);

		scene.add(hemisphereLight, dirLight);

		// Add Skybox
		scene.add(SkyboxHelper.build(hemisphereLight.color));

		sceneDefinition.apply(scene);
	}


	public update(dTime: number): void {
		this.sceneDefinition.update(dTime);

	}

	public get current() : SceneNode | undefined {
		return this.sceneDefinition.current;
	}

	public dispose() {
		this.sceneDefinition.dispose();
	}

}
