import { IDataProvider, IDataProviderResponse } from "../interfaces/IDataProvider";

function waitForMillis(millis: number) : Promise<void> {
	return new Promise(
		(res) => setTimeout(()=>res(), millis)
	);
}



/**
 * A mock implementation.
 */
export default class MockDataProvider implements IDataProvider {
	public async getDataForNode(path: string[]): Promise<IDataProviderResponse> {
		// Fix because on startup "" is passed
		path = path.filter(e => e.length > 0);
		console.log(path);
		await waitForMillis(Math.random() * 1000);

		const response : IDataProviderResponse = {
			responseStructure: "error",
			payload: undefined
		};

		if(path.length === 0) {
			response.responseStructure = "basic";
			response.payload = {
				heading: "Gebäudevisualisierung",
				body: "Hierbei handelt es sich um ein Gebäudevisualisierungstool. Die Szene kann mit beiden Maustasten, sowie dem Mausrad navigiert werden. Gebäude können mit einem Linksklick geöffnet, und mit einem Doppelklick außerhalb des Gebäudes geschlossen werden. Für weitere Infos kann die \"About\"-Seite oben rechts aufgerufen werden."
			};
		} else {
			if(path[0].includes("Building")) {
				handleHouse(path, response);
			} else {
				// error
			}
		}


		return response;
	}

}
function handleHouse(path: string[], res: IDataProviderResponse) {
	const [, level, room] = path;
	if (path.length === 1) {
		res.responseStructure = "basic";
		res.payload = {
			heading: "Gebäude",
			body: "Ein Gebäude wurde angeklickt. Hier könnten kontextrelevante Informationen zu diesem Gebäude stehen."
		};
		return;
	}

	if (path.length === 2) {
		res.responseStructure = "basic";
		res.payload = {
			heading: level,
			body: "Hier können Information zum Stockwerk stehen"
		};
		return;
	}

	const temperatureDeg = 20 + Math.random() * 10;
	const humidityFraction = .4 + Math.random() * .6;

	res.responseStructure = "metrics";
	res.payload = {
		heading: room,
		temperatureDeg,
		humidityFraction
	};
}

function handleGarage(path: string[], res: IDataProviderResponse) {
	const [, room] = path;

	if (path.length === 1) {
		res.responseStructure = "basic";
		res.payload = {
			heading: "Garage",
			body: "Die Garage wurde angeklickt. Hier könnten kontextrelevante Informationen zur Garage stehen."
		};
		return;
	}

	const temperatureDeg = 20 + Math.random() * 10;
	const humidityFraction = .4 + Math.random() * .6;

	res.responseStructure = "metrics";
	res.payload = {
		heading: room,
		temperatureDeg,
		humidityFraction
	};

}


const SingletonInstance = new MockDataProvider();
export {SingletonInstance};
