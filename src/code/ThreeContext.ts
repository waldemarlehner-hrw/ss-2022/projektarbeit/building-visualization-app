import { PCFShadowMap, PerspectiveCamera,  Scene, sRGBEncoding, WebGLRenderer } from "three";
import MainSceneDefinition from "../data/MainSceneDefinition";
import { CameraController } from "./CameraController";
import DoubleClickHandler from "./other/DoubleClickHandler";
import { SceneNode } from "./sceneschema/SceneNode";
import { SceneWrapper } from "./SceneWrapper";
import { Topic } from "./Topic";

/**
 * This Class is the high-level abstraction around the Three Context. It accepts new selected Nodes and emits State-Change events
 */
export class ThreeContext {

	public readonly newSelectableChildrenEvent = new Topic<string[]>();
	public readonly treeLocationChangedEvent = new Topic<string[]>();

	/*
     * Internal Properties
     */
	private readonly sceneWrapper: SceneWrapper;
	private readonly renderer: WebGLRenderer;
	private readonly camera: PerspectiveCamera;

	private voidDoubleClickHandler : DoubleClickHandler;

	constructor(private readonly containerElement: HTMLElement) {

		console.info("Constructing Three Context");
		this.camera = new PerspectiveCamera(75, 1, .1, 1000);
		this.camera.position.set(20, 20, 20);
		const sceneDefinition = MainSceneDefinition();

		this.voidDoubleClickHandler = new DoubleClickHandler(400, 500);
		this.voidDoubleClickHandler.onDoubleClickEvent.addListener( () => this.selectCurrentParent());


		const cameraController = new CameraController(this.camera, containerElement);
		cameraController.elementClickedEvent.addListener((data) => this.handleNewElementClickedEvent(data));
		cameraController.voidClickedEvent.addListener(() => this.voidDoubleClickHandler.notifyAboutClick());
		sceneDefinition.init(cameraController);

		this.sceneWrapper = new SceneWrapper(new Scene() , sceneDefinition);
		this.sceneWrapper.newSelectableChildrenEvent.addListener((data) => this.newSelectableChildrenEvent.invoke(data));

		this.renderer = new WebGLRenderer({antialias:true});
		{
			const r = this.renderer as any; // Properties not Documented with TypeScript
			r.shadowMap.enabled = true;
			r.shadowMap.type = PCFShadowMap;
			r.outputEncoding = sRGBEncoding;
		}

		containerElement.appendChild(this.renderer.domElement);

		window.addEventListener("resize", () => this.triggerRendererResize());

		this.camera.position.z = +5;

		this.triggerRendererResize();
		this.render();
	}

	private selectCurrentParent(): void {
		const current = this.sceneWrapper.current;
		if(!current) {
			console.warn("There is no current SceneNode. Dropping command.");
			return;
		}
		const parent = current.getParent();
		if(!parent) {
			console.info("No Parent Element was found. Dropping command.");
			return;
		}
		this.handleNewElementClickedEvent(parent);
	}

	public notifyAboutNewTreeLocation(path: string[]) {
		const [trimPath, isValid] = this.sceneWrapper.validatePath(path);
		if (!isValid) {
			// The valid path is emitted as the user Input. That way Vue is informed, which then informs the
			// Context about the new path. That way this method is called again, this time with the correct path.
			this.treeLocationChangedEvent.invoke(trimPath);
		} else {
			this.sceneWrapper.setNewSelectedElement(trimPath);
		}
	}

	public dispose() {
		console.info("Starting Dispose");
		this.treeLocationChangedEvent.clear();
		this.sceneWrapper.dispose();

	}

	private handleNewElementClickedEvent(element : SceneNode) {
		const name : string[] = element.getEntirePath();
		this.treeLocationChangedEvent.invoke(name);
	}

	private triggerRendererResize() {
		const w = this.containerElement.offsetWidth;
		const h = this.containerElement.offsetHeight;

		this.renderer.setSize(w,h);
		this.camera.aspect = w/h;
		this.camera.updateProjectionMatrix();
	}

	private lastFrameTimeStamp: number | undefined;

	private render() {
		const now = Date.now();
		let dTime: number | undefined;
		if(typeof this.lastFrameTimeStamp !== "undefined") {
			const dTimeMillis = now - this.lastFrameTimeStamp;
			dTime = dTimeMillis / 1000;
		}

		this.renderer.render(this.sceneWrapper.scene, this.camera);


		if(!!dTime) {
			this.sceneWrapper.update(dTime);
		}
		this.lastFrameTimeStamp = now;
		window.requestAnimationFrame(() => this.render());
	}

}
