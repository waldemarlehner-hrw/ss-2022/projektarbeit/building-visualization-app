import { Vector3 } from "three";
import { SceneNode } from "../sceneschema/SceneNode";
import Animator from "./Animator";

export default class SinewaveAnimator extends Animator {

	private _currentState: "running" | "idle" = "idle";
	private dTimeSinceStart = 0;
	private cb?: () => void = undefined;

	public get currentState(): "running" | "idle" {
		return this._currentState;
	}
	public animate(cb?: () => void): void {
		if(this.currentState === "running") {
			console.error("Received command to animate for target "+this.target.id+", but is already animating");
			return;
		}
		this.cb = cb;
		this._currentState = "running";
		this.dTimeSinceStart = 0;
		this.applyState(0);
	}
	public abort(): void {
		if(this.currentState === "idle") {
			console.warn("Received command to abort animation for target "+this.target.id+", but is already idle");
			return;
		}
		this._currentState = "idle";
		if(!!this.cb) {
			this.cb();
		}
		this.applyState(1);

	}

	public static BuildRelativeChanges(target: SceneNode, relativeStartingPosition: Vector3, duration: number) {
		const to = target.localPosition;
		const from = new Vector3(
			to.x + relativeStartingPosition.x,
			to.y + relativeStartingPosition.y,
			to.z + relativeStartingPosition.z
		);
		return new SinewaveAnimator(target, from, to, duration);
	}

	public static BuildWithExplicitStartPosition(target: SceneNode, explicitStart: Vector3, duration: number) {
		const to = target.localPosition.clone();
		const from = explicitStart;
		return new SinewaveAnimator(target, from, to, duration);
	}

	public static BuildWithExplicitStartPositionGenerator(explicitStart: Vector3, duration: number) {
		return (node: SceneNode) => this.BuildWithExplicitStartPosition(node, explicitStart, duration);
	}

	private constructor(private target: SceneNode, private from: Vector3, private to: Vector3, private duration: number) {
		super();
	}

	public update(dTime: number): void {
		if(this._currentState == "idle") {
			return;
		}
		this.dTimeSinceStart += dTime;
		let currentFraction = this.dTimeSinceStart / this.duration;
		if(currentFraction > 1) {
			currentFraction = 1;
			this._currentState = "idle";
		}

		// See https://www.desmos.com/calculator/aopw6m9qqv
		const currentLinearizedValue = Math.sin((currentFraction-.5)*Math.PI)*.5+.5;
		this.applyState(currentLinearizedValue);

		if(this._currentState === "idle" && !!this.cb) {
			this.cb();
		}

	}

	private applyState(linearizedValue: number) {
		const x = this.from.x + (this.to.x - this.from.x) * linearizedValue;
		const y = this.from.y + (this.to.y - this.from.y) * linearizedValue;
		const z = this.from.z + (this.to.z - this.from.z) * linearizedValue;
		this.target.localPosition.set(x,y,z);

	}

}
