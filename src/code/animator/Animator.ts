import { IUpdateable } from "../interfaces/IUpdateable";

export default abstract class Animator implements IUpdateable {

	public abstract update(dTime: number): void;

	public abstract get currentState() : "running" | "idle";

	public abstract animate(onFinishedCallback?: () => void) : void;
	public abstract abort(): void;

}

