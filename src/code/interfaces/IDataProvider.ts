export interface IDataProviderResponse {
	responseStructure: string
	payload: any
}

export interface IDataProvider {
	getDataForNode(path: string[]) : Promise<IDataProviderResponse>
}
