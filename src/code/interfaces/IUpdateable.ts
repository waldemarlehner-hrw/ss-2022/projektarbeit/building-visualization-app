export interface IUpdateable {
	update(dTime: number) : void
}