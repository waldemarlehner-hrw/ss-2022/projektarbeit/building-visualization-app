import { Scene } from "three";
import { CameraController } from "../CameraController";
import { Topic } from "../Topic";
import { SceneNode } from "./SceneNode";
import { SceneNodeRoot } from "./SceneNodeRoot";

/**
 * Defines what Objects are located where, when they are being rendered, etc
 */
export class SceneDefinition {
	public get current() : SceneNode | undefined {
		return this.sceneData.activeNode;
	}

	validatePath(path: string[]): [string[], boolean] {
		const correctPath: string[] = [];
		let currentNode : SceneNode = this.sceneData;
		for(let depth = 0; depth < path.length; depth++) {
			const nextChildId = [path[depth]];
			const nextNode = currentNode.find(nextChildId);
			if (currentNode === nextNode) {
				return [correctPath, false];
			}
			correctPath.push(nextNode.id);
			currentNode = nextNode;
		}
		return [correctPath, true];
	}
	update(dTime: number) {
		if (!!this._cameraController) {
			this._cameraController.update(dTime);
		}
		this.sceneData.updateRelevant(dTime);
	}

	private _cameraController: CameraController | undefined;
	public newSelectableChildrenEvent = new Topic<string[]>();

	constructor(private sceneData: SceneNodeRoot) {
		sceneData.newInteractablesEvent.addListener( e => {
			if (!!this._cameraController) {
				this._cameraController.setNewIntersectables(e);
			}
		});
		sceneData.newSelectableChildrenEvent.addListener(e => this.newSelectableChildrenEvent.invoke(e.map(f => f.id)));
	}

	public get cameraController() : CameraController | undefined {
		return this._cameraController;
	}

	public init(cameraController: CameraController) {
		if (!this._cameraController) {
			this._cameraController = cameraController;
			this._cameraController.currentlyHoveringSceneNodeTopic.addListener(e => this.sceneData.notifyAboutHoveringNode(e));
		} else {
			throw new Error("Scene Definition is already initialized");
		}
	}

	public findNode(path: string[]) : SceneNode {
		return this.sceneData.find(path);
	}

	public apply(scene: Scene) {
		this.sceneData.linkToScene(scene);
	}

	public dispose() {
		if(!!this._cameraController) {
			this._cameraController.dispose();
			this._cameraController = undefined;
		}
		this.sceneData.dispose();
		this.newSelectableChildrenEvent.clear();
	}

}
