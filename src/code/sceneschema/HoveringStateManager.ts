import { Color,  Mesh, MeshStandardMaterial, Object3D } from "three";


/**
 * This keeps track of which items require an Outline
 */
export default class HoveringStateManager {

	private itemsToOutline: Object3D[] = [];

	private static getAllSubmeshes(item: Object3D, list?: Mesh[]) {
		list = list ?? [];
		if(item instanceof Mesh) {
			list.push(item);
		}
		item.children.forEach( e => this.getAllSubmeshes(e, list));
		return list;
	}

	public addMesh(item: Object3D) {
		if(this.itemsToOutline.includes(item)) {
			return;
		}
		this.itemsToOutline.push(item);
		const submeshes = HoveringStateManager.getAllSubmeshes(item);
		for(const mesh of submeshes) {
			if(!mesh.userData._overlay_material) {
				mesh.userData._overlay_material = (mesh.material as MeshStandardMaterial).clone();
				if (!!mesh.userData._overlay_material.color) {
					mesh.userData._overlay_material.color = new Color(0xFF1010);
				} else {
					console.warn("Cannot create overlay material");
				}
				mesh.userData._material = mesh.material;
			}
			mesh.material = mesh.userData._overlay_material;
		}
	}

	public removeMesh(item: Object3D) : boolean {

		const index = this.itemsToOutline.indexOf(item);
		if(index < 0) {
			return false;
		}
		this.itemsToOutline.splice(index, 1);
		const submeshes = HoveringStateManager.getAllSubmeshes(item);
		for(const mesh of submeshes) {
			mesh.material = mesh.userData._material;
		}
		return true;
	}
}
