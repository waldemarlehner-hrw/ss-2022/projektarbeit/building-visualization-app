import { BackSide, Color, Mesh, MeshBasicMaterial, SphereGeometry } from "three";



export abstract class SkyboxHelper {

	public static build(color: Color) {
		const geometry = new SphereGeometry(500, 32, 15);
		const material = new MeshBasicMaterial({
			side: BackSide,
			color
		});

		return new Mesh(geometry, material);
	}

}
