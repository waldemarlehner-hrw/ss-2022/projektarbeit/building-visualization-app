import { Material, Mesh, Object3D, Scene } from "three";

export abstract class SceneDisposingHelper {

	public static dispose(scene: Scene | undefined) {
		if (!scene) {
			return;
		}

		scene.children.forEach(e => this.disposeEntry(e));
	}

	private static disposeEntry(node: Object3D) {
		node.children.forEach(e => this.disposeEntry(e));

		if(!!node.userData["sceneNode"]) {
			delete node.userData["sceneNode"];
		}

		node.removeFromParent();
		if(node instanceof Mesh) {
			const mats = node.material;
			let materials: Material[];
			if(Array.isArray(mats)) {
				materials = mats;
			} else {
				materials = [mats];
			}

			for (const mat of materials) {
				mat.dispose();
			}

			if(!!node.geometry) {
				node.geometry.dispose();
			}
		}

	}

}
