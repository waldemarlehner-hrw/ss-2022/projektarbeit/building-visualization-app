import { Scene, Group, Object3D, Vector3 } from "three";
import { IUpdateable } from "../interfaces/IUpdateable";
import { Topic } from "../Topic";
import HoveringStateManager from "./HoveringStateManager";
import { SceneDisposingHelper } from "./SceneDisposingHelper";
import { SceneNode } from "./SceneNode";

// The "root" of the scene
export class SceneNodeRoot extends SceneNode {

	private currentHoveringNode : SceneNode | undefined = undefined;
	public readonly outlineStateManager: HoveringStateManager;

	public notifyAboutHoveringNode(node: SceneNode | undefined): void {
		if(this.currentHoveringNode === node) {
			return;
		}

		// if here, a change has happened.
		if(!!this.currentHoveringNode) {
			this.currentHoveringNode.notifyHoverModeStateChange(false);
		}

		this.currentHoveringNode = node;
		if(!!this.currentHoveringNode) {
			this.currentHoveringNode.notifyHoverModeStateChange(true);
		}

	}

	public static Build() {
		const instance = new SceneNodeRoot();
		instance.nodeGroup.userData["node"] = instance as SceneNode;
		return instance;
	}

	public newInteractablesEvent = new Topic<Object3D[]>();
	public newSelectableChildrenEvent = new Topic<SceneNode[]>();

	private updateables: IUpdateable[] = [];

	updateRelevant(dTime: number) {
		this.updateables.forEach( e => e.update(dTime) );
	}


	private _scene: Scene | undefined;
	private _activeNode: SceneNode | undefined;

	public get activeNode() : SceneNode | undefined {
		return this._activeNode;
	}

	protected constructor() {
		super("root", [], new Vector3() );
		this.nodeGroup = new Group();
		this.nodeGroup.name = "root";
		this.outlineStateManager = new HoveringStateManager();
	}

	public addUpdateable(entry: IUpdateable) {
		if(!this.updateables.includes(entry)) {
			this.updateables.push(entry);
		}
	}

	public notifyStopUpdatingUpdateable(entry: IUpdateable) {
		const indexToRemove = this.updateables.indexOf(entry);
		if (indexToRemove === -1) {
			return;
		}

		this.updateables.splice(indexToRemove, 1);
	}

	public notifyAboutNewActiveNode(node: SceneNode, childrenMeshes: Object3D[], children: SceneNode[]) {
		if(!node) {
			throw new Error("Node is undefined!");
		}
		if(this._activeNode === node) {
			return;
		}
		if(!!this._activeNode) {
			this._activeNode.setInactive();
		}
		this._activeNode = node;
		this.newInteractablesEvent.invoke(childrenMeshes);
		this.newSelectableChildrenEvent.invoke(children);
	}

	public linkToScene(scene: Scene) {
		this._scene = scene;
		this._scene.add(this.nodeGroup);
	}

	protected override getScene() : Scene | undefined {
		return this._scene;
	}

	protected override get root(): SceneNodeRoot {
		return this;
	}

	public override dispose(): void {
		SceneDisposingHelper.dispose(this._scene);
		super.dispose();
	}


}
