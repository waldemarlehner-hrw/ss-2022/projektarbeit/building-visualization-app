import { Vector3 } from "three";
import Animator from "../animator/Animator";
import { SceneNode } from "./SceneNode";

export interface ISceneNodeOptions {
	/**
	 * The Node Groups Local Position.
	 * @default new Vector3(0,0,0)
	 */
	localPosition?: Vector3
	/**
	 * When selecting this Node, should displaying its children be blocked?
	 * This is useful for when the Node does not have any children
	 * @default false
	 */
	blockExpansion?: boolean
	/**
	 * When set, this Node becomes a Scenery Node.
	 * This results in it being ignored by the Raycaster.
	 * In addition to that it will no longer be selectable from the UI.
	 * @default false
	 */
	scenery?: boolean
	/**
	 * The Node Groups Local Rotation (XYZ)
	 * @default new Vector3(0,0,0)
	 */
	localRotation?: Vector3
	/**
	 * A function that will create the animator with the SceneNode provided. If none is set no animation is taking place
	 */
	animatorGenerator?: (node: SceneNode) => Animator
}
