/* eslint-disable @typescript-eslint/indent */
import { Euler, Group, Scene, Vector3 } from "three";
import Animator from "../animator/Animator";
import { ISceneNodeOptions } from "./ISceneNodeOptions";
import { LazyModel } from "./modelschema/LazyModel";
import { ModelDefinition } from "./modelschema/ModelDefinition";
import { SceneNodeRoot } from "./SceneNodeRoot";


export class SceneNode {

	public async notifyHoverModeStateChange(isBeingHovered: boolean) {
		const models = await Promise.all(this.models.map(e => e.get()));
		const outlineState = this.root.outlineStateManager;
		if(isBeingHovered) {
			models.forEach( e => outlineState.addMesh(e));
		} else {
			models.forEach( e => outlineState.removeMesh(e));
		}
	}

	public static Build(_id: string, models: ModelDefinition[], options?: ISceneNodeOptions) {
		const instance = new SceneNode(
			_id,
			models,
			options?.localPosition ?? new Vector3(),
			options?.localRotation ?? new Vector3(),
			);
		instance.blockExpansion = options?.blockExpansion ?? false;
		instance.scenery = options?.scenery ?? false;
		instance.nodeGroup.userData["node"] = instance;

		if(!!options?.animatorGenerator) {
			const animator = options.animatorGenerator(instance);
			instance.animator = animator;
		}

		return instance;
	}


	/**
     *
     * @param _id The ID for this Node. Used to find contextual data
     * @param models Model(s) to display when the current selected node is this Node's Parent
     *               Once the Node gets selected everything is hidden but this node's childrens' models
     *               Models can be clickable, in which case this Node gets selected as the Active node
     */
	protected constructor(
		private _id : string,
		models: ModelDefinition[],
		localPosition: Vector3,
		localRotation?: Vector3,
		private animator?: Animator
		) {
		if(!localRotation) {
			localRotation = new Vector3();
		}
		this.nodeGroup = new Group();
		this.nodeGroup.position.copy(localPosition);
		this.nodeGroup.setRotationFromEuler(new Euler(localRotation.x, localRotation.y, localRotation.z));
		this.nodeGroup.name = `${_id}-group`;
		this.models = models.map( e => new LazyModel(e, this.nodeGroup));
	}

	public get localPosition() {
		return this.nodeGroup.position;
	}

	private models: LazyModel[];
	private blockExpansion = false;
	private scenery = false;

	public getParent() {
		return this.parent;
	}

	protected parent: SceneNode | undefined;
	protected children: SceneNode[] = [];
	protected nodeGroup: Group;
	protected isActive = false;

	public setInactive() {
		this.isActive = false;
	}

	private startChildrenPreload(clickableChildren: SceneNode[]) {
		// Preload Children Meshes
		if(import.meta.env.DEV) {
			console.groupCollapsed("Model Preloading");
		}
		clickableChildren.forEach( clickableChild => {
			clickableChild.children.forEach( nodeOfChild => nodeOfChild.models.forEach(model => {
				if(import.meta.env.DEV) {
					console.log("Preloading Model ",model, "belonging to "+clickableChild._id+" because "+this._id+" was selected. Model Status is "+model.status);
				}
				return model.get();
			}));
		});
		if(import.meta.env.DEV) {
			console.groupEnd();
		}
	}


	public async setActive() {
        if (this.isActive === true) {
			console.info("Node is already active. Nothing was done");
			return;
		}

		const pathToRoot = this.getPathToRoot();

		// Check for expansion blocking parents
		let expansionBlocker : SceneNode | undefined;
		for(const entry of pathToRoot) {
			if(entry.blockExpansion) {
				expansionBlocker = entry;
			}
		}

		/**
		 * If a blocker was set, the blocker's parent's children will be visible
		 */
		if(!!expansionBlocker) {
			// Get the parent
			expansionBlocker = expansionBlocker.parent;
		}
		if(!expansionBlocker) {
			// NOT an else block because previous blocker parent might be undefined
			expansionBlocker = this as SceneNode;
		}


		let clickableChildren : SceneNode[] = this.blockExpansion ? (this.parent?.children ?? []) : this.children;
		const childrenToAnimate = [...clickableChildren];

		clickableChildren = clickableChildren.filter(e => !e.scenery);

		this.startChildrenPreload(clickableChildren);

		// Load the Current Children's Models.
		console.log("Clickable Children are:", clickableChildren.map(e => e._id));
		const visibleChildMeshes = await Promise.all(clickableChildren.map(e => {
			return Promise.all(e.models.map(async f => await f.get()));
		}));

		// Start the animation AFTER the Model has loaded.
		this.startAnimation(childrenToAnimate, expansionBlocker);

		this.root.notifyAboutNewActiveNode(this, visibleChildMeshes.flat(), clickableChildren);

		// Traverse from this Node (or the blocker's parent) all the way up and make every child hidden
		const fromBlockerOrSelfToRoot = expansionBlocker.getPathToRoot();

		// the parent of root is undefined and will be the escape condition
		fromBlockerOrSelfToRoot.forEach( e => e.children.forEach(f => f.nodeGroup.visible = false));

		// Go through again from currentNode (or blocker's parent) to root and make the path visible
		fromBlockerOrSelfToRoot.forEach( e => e.nodeGroup.visible = true);

		// Make the Child Groups visible, but hide any children of the children
		expansionBlocker.children.forEach(async e => {
			e.nodeGroup.visible = true;
			e.children.forEach(f => f.nodeGroup.visible = false);
			// The Models of each Child should be visible
			const models = await Promise.all(e.models.map(m => m.get()));
			models.forEach(m => m.visible = true);
		});

		// Make this node's Models invisible
		Promise.all(expansionBlocker.models.map(e => e.get())).then(models => models.forEach(
			e => e.visible = false
		));
	}
	private startAnimation(childrenToAnimate: SceneNode[], expansionBlocker: SceneNode) {
			// Animate all the Children IF the current node is not an expansion blocker.
			if(expansionBlocker === this) {
				childrenToAnimate.forEach( e => {
					if(!!e.animator && e.animator.currentState === "idle") {
						e.root.addUpdateable(e.animator);
						e.animator.animate(()=>this.root.notifyStopUpdatingUpdateable(e.animator!));
					}
				});
				// If an animator is present, make use of it
			}

	}

	public getEntirePath(): string[] {
		const sceneNodeChain: SceneNode[] = [];
		// eslint-disable-next-line @typescript-eslint/no-this-alias
		let current: SceneNode = this;
		while(!!current.parent) {
			sceneNodeChain.unshift(current);
			current = current.parent;
		}

		return sceneNodeChain.map(e => e._id);
	}

	protected get root(): SceneNodeRoot {
		if (!!this.parent) {
			return this.parent.root;
		} else {
			throw new Error("This Node is an orphan!");
		}
	}

	public addChild(node: SceneNode) {
		this.addChildren([node]);
	}

	public addChildren(nodes: SceneNode[]) {
		for(const node of nodes) {
			if(!!node.parent) {
				console.warn("Reassigning Parent. This should not happen");
				const index = node.parent.children.indexOf(node);
				node.parent.children.splice(index, 1);
				node.nodeGroup.removeFromParent();
			}
			node.parent = this;
			this.nodeGroup.add(node.nodeGroup);
			this.children.push(node);
		}
	}

	public get id() : string {
		return this._id;
	}

	/**
     * The context ID is an identifier that is used to query data about the currently selected object.
     * The queried data can be displayed in the Vue Sidebar
     */
	public get contextId() : string | undefined {
		return this.getEntirePath().join(".");
	}

	public find(path: string[]) : SceneNode {
		if(path.length == 0) {
			return this;
		}
		const currentPath = path[0];
		let foundChild: SceneNode | undefined;
		for(const entry of this.children) {
			if(entry.id == currentPath) {
				foundChild = entry;
				break;
			}
		}

		if(!foundChild) {
			return this;
		}
		path.splice(0,1);
		return foundChild.find(path);
	}

	protected getScene(): Scene | undefined {
		if (!!this.parent) {
			return this.parent.getScene();
		} else return undefined;
	}

	private getPathToRoot(currentArray?: SceneNode[]) : SceneNode[] {
		if(!currentArray) {
			currentArray = [];
		}
		currentArray.push(this);
		if (!this.parent) {
			return currentArray;
		}
		return this.parent.getPathToRoot(currentArray);
	}


	public dispose() {
		for(const child of this.children) {
			child.dispose();
		}
		// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
		this.nodeGroup = undefined!;
		this.models.forEach( e => e.dispose() );
	}
}


