import { Mesh, Object3D } from "three";
import { ModelDefinition } from "./ModelDefinition";

export class LazyModel {
	private _instance : Object3D | undefined;

	private gettingPromise: Promise<Object3D> | undefined;
	public get status() {
		if (!!this._instance) {
			return "loaded";
		}
		if (!!this.gettingPromise) {
			return "loading";
		}
		return "not loaded";
	}

	constructor(private modelDefinition: ModelDefinition, private parent: Object3D) {}

	public get isInit() : boolean {
		return !!this._instance;
	}

	private async makePromise() : Promise<Object3D> {
		if (!this._instance) {
			this._instance = await this.modelDefinition.construct(this.parent);
			if(!this._instance) {
				throw new Error("Model could not be built.");
			}
			console.log(`Added Model to ${this.parent.name}`);
		}
		return this._instance;
	}

	public get() : Promise<Object3D> {
		if (this._instance) {
			return Promise.resolve(this._instance);
		}
		if (!this.gettingPromise) {
			this.gettingPromise = this.makePromise();
		}
		if (typeof this.gettingPromise === "undefined") {
			throw new Error("Promise is undefined. This should never happen");
		}
		return this.gettingPromise;
	}

	/**
     * Three.JS does not have anything like a Garbage Collector.
     * The user has to free up memory themselves. This will clear up Buffers on the GPU
     */
	public dispose() {
		if (!!this._instance) {
			if(this._instance instanceof Mesh) {
				this._instance.geometry.dispose();
				const mats = this._instance.material instanceof Array ?
					this._instance.material :
					[this._instance.material];
				for( const mat of mats) {
					mat.dispose();
				}
				this.parent.remove(this._instance);
			}
			console.info("Disposed Model");
		}
		this._instance = undefined;
	}
}
