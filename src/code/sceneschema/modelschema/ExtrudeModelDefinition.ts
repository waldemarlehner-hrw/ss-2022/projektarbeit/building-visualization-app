import { Mesh, BufferGeometry, Material, Vector3, MeshLambertMaterial, Vector2, Shape, ExtrudeGeometry, Object3D } from "three";
import { ModelDefinition } from "./ModelDefinition";

export class ExtrudeModelDefinition implements ModelDefinition {

	public static fromJSON(data: any) : ModelDefinition {
		throw new Error("Method not implemented.");
	}

	private geometry: ExtrudeGeometry;
	private material: Material;

	constructor(base: Vector2[], height: number, private positionRelativeToParent: Vector3, material?: Material) {

		if (base.length < 3) {
			throw new Error("Base needs to be at least of length 3");
		}

		const shape = new Shape();

		// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
		const firstElement = base.shift()!;


		shape.moveTo(firstElement.x, firstElement.y);

		for(const entry of base) {
			shape.lineTo(entry.x, entry.y);
		}
		shape.lineTo(firstElement.x, firstElement.y);

		const extrudeSettings = {
			steps: 1,
			depth: height,
			bevelEnabled: false
		};

		this.geometry = new ExtrudeGeometry(shape, extrudeSettings);
		this.material = material ?? new MeshLambertMaterial({color: Math.random() * 0xFFFFFF});

	}

	public get type(): "extrude" {
		return "extrude";
	}
	public async construct(parent: Object3D): Promise<Mesh<BufferGeometry, Material | Material[]>> {
		const mesh =  new Mesh(this.geometry, this.material);
		parent.add(mesh);
		mesh.position.set(
			this.positionRelativeToParent.x,
			this.positionRelativeToParent.y,
			this.positionRelativeToParent.z,
		);
		mesh.rotateX(-Math.PI/2);
		return mesh;
	}

}
