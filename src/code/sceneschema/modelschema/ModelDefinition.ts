import { Object3D } from "three";
import { ExtrudeModelDefinition } from "./ExtrudeModelDefinition";
import { ModelDefinitionJson } from "./json/ModelDefinitionJSON";

export abstract class ModelDefinition {
	public abstract get type() : "box" | "extrude" | "model"
	public abstract construct(parent: Object3D) : Promise<Object3D | undefined>

	public static fromJSON(json: ModelDefinitionJson) : ModelDefinition {
		if(json.type === "extrude") {
			ExtrudeModelDefinition.fromJSON(json.data);
		}
		throw new Error("Not Implemented");
	}
}
