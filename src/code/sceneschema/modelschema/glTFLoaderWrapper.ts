import { Object3D } from "three";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";

function setShadowsTrue(root: Object3D) {
	root.castShadow = true;
	root.children.forEach( e => setShadowsTrue(e));
}


// Acts as a Singleton
const loader = new GLTFLoader();

async function load(modelUrl: string) {
	try {
		const gltf = await loader.loadAsync(modelUrl);
		const scene = gltf.scene;
		setShadowsTrue(scene);
		return scene;
	} catch (err) {
		console.error(err);
		return undefined;
	}
}

export { load };
