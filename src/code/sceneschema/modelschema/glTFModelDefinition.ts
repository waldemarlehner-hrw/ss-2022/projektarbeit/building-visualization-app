import { Object3D, Event, Vector3, Group } from "three";
import { ModelDefinition } from "./ModelDefinition";
import { load as loadModel } from "./glTFLoaderWrapper";

export default class GLTFModelDefinition implements ModelDefinition {

	constructor(private gltfModelPath: string, private positionRelativeToParent?: Vector3) {
	}

	public get type(): "box" | "extrude" | "model" {
		return "model";
	}

	public async construct(parent: Object3D<Event>): Promise< Group | undefined> {
		const model = await loadModel(this.gltfModelPath);
		if(!model) {
			return undefined;
		}

		parent.add(model);

		if(!!this.positionRelativeToParent) {
			const p = this.positionRelativeToParent;
			model.position.set(p.x, p.y, p.z);
		}


		return model;

	}

}
