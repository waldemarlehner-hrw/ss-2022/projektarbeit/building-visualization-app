import { IUpdateable } from "../interfaces/IUpdateable";

export interface ISceneNodeAnimator extends IUpdateable {

	get awaitAnimationFinished() : Promise<undefined>

}

