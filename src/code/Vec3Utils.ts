import { Vector3 } from "three";

export abstract class Vec3Utils {
	static delta(from: Vector3, to: Vector3) {
		return new Vector3(
			from.x - to.x,
			from.z - to.z,
			from.y - to.y,
		);
	}
	public static absDelta(a: Vector3, b: Vector3) {
		return new Vector3(
			Math.abs(a.x - b.x),
			Math.abs(a.y - b.y),
			Math.abs(a.z - b.z),
		);
	}
	public static add(vec1 : Vector3, vec2 : Vector3) : Vector3 {
		return new Vector3(
			vec1.x + vec2.x,
			vec1.y + vec2.y,
			vec1.z + vec2.z,
		);
	}

	public static average(...vecs: Vector3[]) : Vector3 {
		const len = vecs.length;
		let x = 0, y = 0, z = 0;
		for( const v of vecs) {
			x += v.x;
			y += v.y;
			z += v.z;
		}

		return new Vector3(
			x / len, 
			y / len, 
			z / len, 
		);
	}
}