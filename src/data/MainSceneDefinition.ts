/**
 * This defines what should be rendered. In the future, this might be loaded in as JSON
 */

import { Vector3 } from "three";
import SinewaveAnimator from "../code/animator/SinewaveAnimator";
import { ISceneNodeOptions } from "../code/sceneschema/ISceneNodeOptions";
import GLTFModelDefinition from "../code/sceneschema/modelschema/glTFModelDefinition";
import { SceneDefinition } from "../code/sceneschema/SceneDefinition";
import { SceneNode } from "../code/sceneschema/SceneNode";
import { SceneNodeRoot } from "../code/sceneschema/SceneNodeRoot";

import * as ModelPathProvider from "./models/ModelPathProvider";


function building1Floor1(b1: SceneNode) {
	const floor = ModelPathProvider.B1.floor1;

	const b1f1 = SceneNode.Build("Floor 1", [new GLTFModelDefinition(floor.Model)], {
		localPosition: new Vector3(0,0,0)
	});

	const roomPositionsOffset: [Vector3, Vector3][] = [
		[new Vector3(0,0.2,-20)		, new Vector3(0,0,-1)	],
		[new Vector3(0,0.0,-6.54)	, new Vector3(-.5,0,0)    ],
		[new Vector3(7,0,-6.54)		, new Vector3(.5,0,0)    ],
		[new Vector3(0,0,0)			, new Vector3(0,0,1)    ],
		[new Vector3(0,0,0)			, new Vector3(-1,0,2)    ],
	];

	const roomPositions = roomPositionsOffset.map( ([a,b]) => [a, new Vector3(a.x+b.x, a.y+b.y, a.z+b.z)]);

	const models = [floor.R1, floor.R2, floor.R3, floor.R4, floor.R5];

	const rooms = roomPositions.map( ([from, to], i) =>
		SceneNode.Build("Room "+(i+1), [new GLTFModelDefinition(models[i])], {
			localPosition: to,
			animatorGenerator: SinewaveAnimator.BuildWithExplicitStartPositionGenerator(from, .5),
			blockExpansion: true
		})
	);

	b1f1.addChildren(rooms);
	b1.addChild(b1f1);
}

function building1Floor2(b1: SceneNode) {
	const floor = ModelPathProvider.B1.floor2;
	const b1f2 = SceneNode.Build("Floor 2", [new GLTFModelDefinition(ModelPathProvider.B1.floor2.Model)], {
		localPosition: new Vector3(0,8,0),
		animatorGenerator: SinewaveAnimator.BuildWithExplicitStartPositionGenerator(new Vector3(0,3.89,0), .5)
	});

	const roomPositionsOffset: [Vector3, Vector3][] = [
		[new Vector3(0,0,-13.655)		, new Vector3(-1, 0, -2)	   ],
		[new Vector3(0,0,-6.54)	, new Vector3(-1, 0, -1)    ],
		[new Vector3(0,0,-3.55)		, new Vector3()    ],
		[new Vector3(0,0,0)			, new Vector3(0,0,1)    ],
		[new Vector3(9.81,0,-7.24)			, new Vector3(1, 0, 1)    ],
		[new Vector3(9.81,0,-6.54)			, new Vector3(1, 0, 0)    ],
		[new Vector3(9.81,0,-13.655)			, new Vector3(1, 0, -1)    ],
	];

	const roomPositions = roomPositionsOffset.map( ([a,b]) => [a, new Vector3(a.x+b.x, a.y+b.y, a.z+b.z)]);

	const models = [floor.R1, floor.R2, floor.R3, floor.R4, floor.R5, floor.R6, floor.R7];

	const rooms = roomPositions.map( ([from, to], i) =>
		SceneNode.Build("Room "+(i+1), [new GLTFModelDefinition(models[i])], {
			localPosition: to,
			animatorGenerator: SinewaveAnimator.BuildWithExplicitStartPositionGenerator(from, .5),
			blockExpansion: true,
			localRotation: i!=4?undefined:new Vector3(0,Math.PI,0)
		})
	);

	b1f2.addChildren(rooms);
	b1.addChild(b1f2);

}




function building2(root: SceneNodeRoot) {
	const b2 = SceneNode.Build("Secondary Building", [new GLTFModelDefinition(ModelPathProvider.B1.Model)], {
		localPosition: new Vector3(-8, 0,-20),
		localRotation: new Vector3(0, Math.PI, 0)
	});
	building1Floor1(b2);
	building1Floor2(b2);


	const b1rt = SceneNode.Build("Roof", [new GLTFModelDefinition(ModelPathProvider.B1.Rooftop)], {
		localPosition: new Vector3(0,15,0),
		animatorGenerator: SinewaveAnimator.BuildWithExplicitStartPositionGenerator(new Vector3(0, 6.59, 0), .5),
		scenery: true
	});

	b2.addChild(b1rt);
	root.addChild(b2);
}



function building1(root: SceneNodeRoot, opt?: ISceneNodeOptions) {
	const b1 = SceneNode.Build("Main Building", [new GLTFModelDefinition(ModelPathProvider.B1.Model)], opt);
	building1Floor1(b1);
	building1Floor2(b1);


	const b1rt = SceneNode.Build("Roof", [new GLTFModelDefinition(ModelPathProvider.B1.Rooftop)], {
		localPosition: new Vector3(0,15,0),
		animatorGenerator: SinewaveAnimator.BuildWithExplicitStartPositionGenerator(new Vector3(0, 6.59, 0), .5),
		scenery: true
	});

	b1.addChild(b1rt);
	root.addChild(b1);
}



export default () => {
	console.info("Building Main Scene Definition");

	const sceneRoot =  SceneNodeRoot.Build();

	building1(sceneRoot);
	building2(sceneRoot);
	//buildHouse1(sceneRoot);
	//buildHouse2(sceneRoot);
	//sceneRoot.addChild(buildScenery());
	// add road
	sceneRoot.addChild(SceneNode.Build("road_scenery", [new GLTFModelDefinition(ModelPathProvider.Road)], {scenery: true, localPosition: new Vector3(-8,0,0)}))

	return new SceneDefinition(sceneRoot);
};
