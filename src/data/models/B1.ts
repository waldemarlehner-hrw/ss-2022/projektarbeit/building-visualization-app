import B1 from "./B1.glb?url";

import B1F1 from "./B1F1.glb?url";
import B1F2 from "./B1F2.glb?url";
import B1RT from "./B1RT.glb?url";

import B1F1R1 from "./B1F1R1.glb?url";
import B1F1R2 from "./B1F1R2.glb?url";
import B1F1R3 from "./B1F1R3.glb?url";
import B1F1R4 from "./B1F1R4.glb?url";
import B1F1R5 from "./B1F1R5.glb?url";

import B1F2R1 from "./B1F2R1.glb?url";
import B1F2R2 from "./B1F2R2.glb?url";
import B1F2R3 from "./B1F2R3.glb?url";
import B1F2R4 from "./B1F2R4.glb?url";
import B1F2R5 from "./B1F2R5.glb?url";
import B1F2R6 from "./B1F2R6.glb?url";
import B1F2R7 from "./B1F2R7.glb?url";

export const floor1 = {
	Model: B1F1,
	R1: B1F1R1,
	R2: B1F1R2,
	R3: B1F1R3,
	R4: B1F1R4,
	R5: B1F1R5,
};

export const floor2 = {
	Model: B1F2,
	R1: B1F2R1,
	R2: B1F2R2,
	R3: B1F2R3,
	R4: B1F2R4,
	R5: B1F2R5,
	R6: B1F2R6,
	R7: B1F2R7,
};

export const Rooftop = B1RT;


export const Model = B1;
