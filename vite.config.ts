import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import {transformAssetUrls, quasar} from "@quasar/vite-plugin";
import eslint from "vite-plugin-eslint";

function getBase(useGitlab: boolean){
	if(useGitlab) {
		return process.env.CI_PAGES_URL+"/";
	}
	return "/";
}

// https://vitejs.dev/config/
export default defineConfig({
	plugins: [
		eslint(),
		vue({
			template: { transformAssetUrls }
		}),
		quasar({
			autoImportComponentCase: "combined",
			sassVariables: "./src/quasar-variables.sass"
		})
	],
	base: getBase(process.env.GITLAB_BUILD == "1"),
	build: {
		rollupOptions: {
			output: {
				manualChunks(id) {
					if (id.includes("node_modules")) {
						if (id.includes("vue")) {
							return "vendor-vue";
						}
						if (id.includes("three")) {
							return "vendor-three";
						}
					}
				}
			}
		}
	}

});
